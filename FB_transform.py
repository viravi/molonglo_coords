import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, Angle, FK5
import pyfits

# in fan beam frame, let x be EW at the meridian, and y be NS. 

# burst properties

lst = Angle('06:39:47.6451 hours') # lst of burst
time = Time('2016-03-17 00:14:09')-11.*u.hour # utc burst time
coords = SkyCoord('07:53:56.05 -29:38:12.8', unit=(u.hourangle,u.deg), frame='fk5') # coord (J2000) of middle of centre beam - assume no NS pointing error
fan_beam = 212 # fan beam number
hphw_az = 1.22*(3e8/8.43e8)/1500./2. # half-power half-width in az direction
hphw_perp = 1.22*(3e8/8.43e8)/11./2. # half-power half-width in perpendicular direction

# get ha
burst_time_approx = 'J'+str(2016.+77./366.25) # for precession/nutation of J2000 coords
ha = lst - coords.transform_to(FK5(equinox=burst_time_approx)).ra
dec = coords.transform_to(FK5(equinox=burst_time_approx)).dec

# get az and el of fan beam centre
molonglo = EarthLocation(lon=149.424658*u.deg, lat=-35.370707*u.deg, height=200*u.m)
aaz = coords.transform_to(AltAz(obstime=time,location=molonglo))
aaz_beam = AltAz(az=Angle(aaz.az)+Angle((fan_beam-176.)*4./352.,unit=u.deg),alt=aaz.alt,obstime=time,location=molonglo)

# get angle between az and zenith line
a = np.pi/2.-aaz.alt.radian
b = np.pi/2.-aaz.alt.radian
c = np.abs(np.pi-aaz.az.radian)*np.cos(aaz.alt.radian)
ang = np.arccos((np.cos(a)-np.cos(b)*np.cos(c))/(np.sin(b)*np.sin(c)))


# get offsets for FWHMs
aaz_b1 = AltAz(az=Angle(aaz_beam.az)-Angle(hphw_az,unit=u.rad),alt=aaz.alt,obstime=time,location=molonglo)
aaz_b2 = AltAz(az=Angle(aaz_beam.az)+Angle(hphw_az,unit=u.rad),alt=aaz.alt,obstime=time,location=molonglo)
aaz_b3 = AltAz(az=Angle(aaz_beam.az)+Angle(np.cos(ang)*hphw_perp,unit=u.rad),alt=Angle(aaz.alt)+Angle(np.sin(ang)*hphw_perp,unit=u.rad),obstime=time,location=molonglo)
aaz_b4 = AltAz(az=Angle(aaz_beam.az)-Angle(np.cos(ang)*hphw_perp,unit=u.rad),alt=Angle(aaz.alt)-Angle(np.sin(ang)*hphw_perp,unit=u.rad),obstime=time,location=molonglo)

# transform to ra and dec
beam_rd = aaz_beam.transform_to(FK5(equinox='J2000'))
beam_1 = aaz_b1.transform_to(FK5(equinox='J2000'))
beam_2 = aaz_b2.transform_to(FK5(equinox='J2000'))
beam_3 = aaz_b3.transform_to(FK5(equinox='J2000'))
beam_4 = aaz_b4.transform_to(FK5(equinox='J2000'))

print beam_rd.ra.degree,beam_rd.dec.degree
print beam_1.ra.degree,beam_1.dec.degree
print beam_2.ra.degree,beam_2.dec.degree
print beam_3.ra.degree,beam_3.dec.degree
print beam_4.ra.degree,beam_4.dec.degree

# display on image
im = pyfits.open('skv8938767846874.fits')
data = im[0].data
cd1 = im[0].header['CDELT1']
cd2 = im[0].header['CDELT2']
cx1 = im[0].header['CRPIX1']
cx2 = im[0].header['CRPIX2']
cv1 = im[0].header['CRVAL1']
cv2 = im[0].header['CRVAL2']
ln = data.shape[0]

mn = np.mean(data)
std = np.std(data)
plt.imshow(data,extent=[(0.-cx1)*cd1+cv1,(ln-cx1)*cd1+cv1,(0.-cx2)*cd2+cv2,(ln-cx2)*cd2+cv2],vmin=mn-std,vmax=mn+3.*std,interpolation='None',cmap='cool')

ras = np.zeros(5)
decs = np.zeros(5)
ras1 = np.zeros(1)
decs1 = np.zeros(1)
ras[0] = beam_1.ra.degree
ras[1] = beam_2.ra.degree
ras[2] = beam_3.ra.degree
ras[3] = beam_4.ra.degree
ras[4] = beam_1.ra.degree
decs[0] = beam_1.dec.degree
decs[1] = beam_2.dec.degree
decs[2] = beam_3.dec.degree
decs[3] = beam_4.dec.degree
decs[4] = beam_1.dec.degree
ras1[0] = beam_rd.ra.degree
decs1[0] = beam_rd.dec.degree

plt.plot(ras,decs,'ro')
plt.plot(ras1,decs1,'bo')
plt.show()







