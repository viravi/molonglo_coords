# README #

Description of FB_transform.py

### How to run ###

* run FB_transform.py ! I'm not sure if it's right though - certainly some values in the code will need to be updated.

### Outputs ###

* print output of 5 ra and dec values, which are: the centre of the FRB fan beam, and four points at the beam half-power points
* display of these points on a DSS2-blue image (skv8938767846874.fits)

### Things to update ###

* lst, coords, fan_beam, time, hphw_az, hphw_perp at top of code
